## Angular JS

### Creating the Angular JS environnement
 - start apache server
 - create folder `myAngularApp` at the root of the http server (this will be the root of the application: `D:\Program\XAMPPvc15\htdocs` )
 -  Download Angular JS at `https://angularjs.org`, unzip Angular JS to the root folder and **rename** it to `Scripts`

### Add NodeJS
- Download NodeJS at `https://nodejs.org/`
- Launch a terminal :
 
  `node -v` to display node's version
  
  `node` to launch node in the terminal
  
  `.exit` to quit
  
- Lauch the server in the terminal:
  `node server.js`.
  test it in the browser : http://127.0.0.1:8088
  
### Add Express JS
- In the server directory, launch: `npm install express`, there will be a `node-module` folder in which we can find `express` folder.
- Add templates. Add a new module ejs: `npm install ejs`.
- Add a folder in my directory: `view`

- install nodemon :`npm install nodemon -g`.

	Pour lancer le serveur il suffira de faire `nodemon serveur.js`
L'avantage c'est qu'une fois le serveur lancé, a chaque modification des fichiers (surtout du serveur), le serveur se relance tout seul, donc plus besoin de s'amuser à le relancer à chaque fois.
Il se peut qu'il vous demande un fichier package.json pour le lancer, il suffira de faire un `npm init`, et faudra remplir des trucs, pour name vous mettez ce que voulez, par exemple le nom du dossier, version faites entrée y a pas besoin, pour tous les autres trucs aussi faites entrée sauf pour ENTRY POINT où la faut mettre server.js

### Add a Database access
- Use the `pg` module for PostgreSQL: `npm install pg`

### Play with all
- Install express-generator: `npm install express-generator -g`
- create the project with express in the command window: `express prweb`
- Then install dependencies
```
cd prweb
npm install
```
- We will also need the database module: `npm install pg`
- Running npm: `npm start`
- Open in the browser: http://127.0.0.1:3000