// Add http lib
// var http = require('http');
// var EventEmitter = require('events').EventEmitter;
// load url
// var url = require('url');
// var querystring = require('querystring');
 
// Create Server
// var server = http.createServer(function(req, res) {
	// res.writeHead(200);   //a status 200 response(tell it's OK)
	// res.end('Salut tout le monde !');  //Defines the response : the string 
// if ((url.parse(req.url).pathname=="/") || (url.parse(req.url).pathname=="/Hello")){
		// res.writeHead(200,{"Content-Type": "text/html"});
		// res.write('<!DOCTYPE html>'+
	// '<html>'+
	// '	<head>'+
	// '		<meta charset="utf-8" />'+
	// '		<title>Node JS test page</title>'+
	// '	</head>'+
	// '	<body>'+
	// '		<p>Hello from Node.</p>'+
	// '	</body>'+
	// '</html>');
// } else {
		// res.writeHead(404,{"Content-Type": "text/html"});
		// res.write('<!DOCTYPE html>'+
	// '<html>'+
	// '	<head>'+
	// '		<meta charset="utf-8" />'+
	// '		<title>404 status</title>'+
	// '	</head>'+
	// '	<body>'+
	// '		<p>File not found!</p>'+
	// '	</body>'+
	// '</html>');		
// }
	// console.log(url.parse(req.url).pathname);
// });

	// var theURL = url.parse(req.url);
	// var pathname = theURL.pathname;
	// var params = querystring.parse(theURL.query);		
	// console.log(theURL);
	// console.log(params);
	// res.writeHead(200, {"Content-Type": "text/html"});
	// res.write('<DOCTYPE html>');
	// res.write('<html>');
	// res.write(' <head>');
	// res.write('		<meta charset="utf-8" />');
	// res.write('		<title>Node JS test page</title>');
	// res.write('	</head>');
	// res.write('	<body>');
	// res.write('		<p>Hello path is '+pathname+'.</p>');
// if (('firstName' in params) && ('lastName' in params)) {
		// res.write('		<p>Hello '+params['firstName']+' '+params['lastName']+'.</p>');
	// } else {
		// res.write('		<p>Who are you?</p>');
	// }
	// res.write('	</body>');
	// res.write('</html>');
	// res.end();
// });

// Create objects
// var server = http.createServer();
// var dataChanger = new EventEmitter();

// Define request callback
// server.on('request', function(req, res) {
	// dataChanger.emit('changedata', 10);
	
	// res.writeHead(200, {"Content-Type": "text/html"});
	// res.write('<!DOCTYPE html>');
	// res.write('<html>');
	// res.write('	<head>');
	// res.write('		<meta charset="utf-8" />');
	// res.write('		<title>Node JS test page</title>');
	// res.write('	</head>');
	// res.write('	<body>');
	// res.write('		<p>Hello data='+data+'.</p>');
	// res.write('	</body>');
	// res.write('</html>');
	
// });  

// Listen port 08
// server.listen(8088);

// define dataChanger callback
// var data=1;
// dataChanger.on('changedata', function(value){
	// if (data % 2 ==1) {
		// data = value;
	// } else {
		// data = data + 1;
	// }
// });

// Add modules
// var http = require('http');
// var url = require('url');
// var path = require('path');
// var fs = require('fs');
// Create server
// var server = http.createServer();
// Define request callback
// server.on('request', function(req, res) {
	// Use request
	// var theURL = url.parse(req.url);
	// var pathname = `.${theURL.pathname}`;
	// Recognize mime types
	// const mimeType = {
		// '.html': 'text/html',
		// '.ico': 'image/x-icon',
		// '.js': 'text/javascript',
		// '.json': 'application/json',
		// '.css': 'texr/css',
		// '.png': 'image/png',
		// '.jpg': 'image/jpeg',
		// ',gif': 'image/gif'
	// };
	// fs.exists(pathname, function(exist){
		// check if path exists
		// if(!exist){
			// Not found, send 404
			// res.statusCode=404;
			// res.end(`File ${pathname} not found:`);
			// return;
		// }		
		// if(fs.statSync(pathname).isDirectory()){
			// If it is a directory, try with index,html
			// pathname += ' /index.html';
		// }		
		// fs.readFile(pathname, function(err,data){
			// read file and send it
			// if(err) {
				// Errer reading
				// res.statusCode=500;
				// res.end(`Error getting the file: ${err}.`);
			// } else {
				// extract extension and send the file with the mime type, text/plain otherwise
				// var ext = path.parse(pathname).exit;
				// res.setHeader(`Content-type`, mimeType[ext] ||`text/plain` );
				// res.end(data);
			// }
		// });		
	// });
// });

// Express JS
// Add modules
var express = require('express');
// var path = require('path');
// var fs = require('fs');
// Create app
var app = express();

// Set static directory as public
app.use(express.static('public'));

// PostgrSQL connection
var pg = require("pg");
var conString = 'postgres://postgres:768528@localhost:5432/prweb';

// Define app callback 
// app.get('/', function(req, res) {
	// res.writeHead(200,{'Content-Type':'text/html'});
	// fs.readFile('index.html', function (err, data) {
		// res.end(data);
	// });
// });

// Define routes
app.get('/',function(req, res) {
	res.render('index.ejs');
});

app.get('/Hello/all', function(req, res) {
	var names = ['JRR Tolkien', 'David EDDINGS', 'Issac ASIMOV'];
	res.render('myView2.ejs', {names: names});
});

app.get('/Hello/:lastName', function(req,res) {
	res.render('myView.ejs', {lastName: req.params.lastName, firstName:''});
});

app.get('/Hello/:lastName/:firstName', function(req, res) {
	res.render('myView.ejs', {lastName: req.params.lastName, firstName: req.params.firstName});
});

app.get('/List', function(req, res){
	var client = new pg.Client(conString);
	client.connect(function(err){
		if(err) {
			console.error('could not connect to postgres', err);
			res.render('displayItems.ejs', {items:[]});
		} else {
			client.query('SELECT * FROM item ORDER BY id', function(err, result) {
				if (err) {
					console.error('could not connect to postgres', err);
					res.render('displayItems.ejs',{items:[]});
				} else {
					var items = [];
					for (var ind in result.rows) {
						items.push(result.rows[ind]);
					}
					res.render('displayItems.ejs', {items: items});
				}
			
				client.end();
			});
		}
	});
});

app.use(function(req, res, next){
	res.setHeader('Content-Type','text/plain');
	res.status(404).end('File not found!');
});
// Listen port 08
app.listen(8088);