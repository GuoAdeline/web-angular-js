/* ------------------------------------------------
	main Javascript file 
	----------------------------------------------- */
	
// Declare application
var app = angular.module('myAngularApp', ['auctionModule']);

// Declare auctionModule and its controller
var auctionModule = angular.module('auctionModule',[]);
auctionModule.controller('auctionController',['$scope', function($scope){
	$scope.items = [];
	
	$scope.addAnItem = function (item){
		$scope.items.push(item);
	};
	
	$scope.addAnItem ({
		title: "Computer",
		author: "J.Y. Martin",
		body: "I sell my old computer, 3 years old"
	});
	$scope.addAnItem ({
		title: "Anime",
		author: "J.Y. Martin",
		body: "Looking for the end of the Fairy Tail manga ( > 126)"
	});
	$scope.addAnItem ({
		title: "Elenium",
		author: "JY Martin",
		body: "I am looking for the french version of the The Elenium series By David Eddings"
	});
	$scope.addAnItem ({
		title: "Kinect",
		author: "JM Normand",
		body: "I sell my new kinect that I can''t connect to my computer"
	});
	$scope.addAnItem ({
		title: "Kikou",
		author: "M Servieres",
		body: "My dog Kikou gave me plenty of little dogs, who wants one?"
	});
	$scope.addAnItem ({
		title: "Mangas",
		author: "M Magnin",
		body: "I am looking for the first Alabator Mangas. Anyone get it?"
	});
	
	$scope.addItem = function () {
		var titleItem = document.getElementById("title");
		var titleValue = titleItem.value;
		
		var authorItem = document.getElementById("author");
		var authorValue = authorItem.value;
		
		var bodyItem = document.getElementById("body");
		var bodyValue = bodyItem.value;
		
		$scope.addAnItem({
			title: titleValue,
			author: authorValue,
			body: bodyValue
		});
	};
	
	// Function to remove the item 'item' in the scope
	$scope.removeItem = function (item) {
		var index = $scope.items.indexOf(item);
		$scope.items.splice(index,1);
	};
}]);

