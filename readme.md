## Rapport Angular JS

#### <center>Step 1 : Angular</center>
**P18**
- **What’s the result? Take a screenshot.**

<img src="./figs/1P18_1.jpg"  style="display:block; max-width:500px;">

**P20**
- **Change item’s display so that we display all of its attributes. It may look like the following:**

![](figs/1P20_1.jpg)

In index.html:
```
<body ng-controller="auctionController">
	The {{item.title}} id sold by {{item.author}}: {{item.body}}
</body>
```
 
**P27**
-	**Add a screenshot of your browser with the auctions** 
	* **As it is in the list:**
  `<tr ng-repeat="item in items">`
  
  ![](./figs/1P27_1.jpg)
  
	* **Ordered by the seller’s name:**
`<tr ng-repeat="item in items | orderBy: 'author'">`

 ![](./figs/1P27_2.jpg)
 
    * **Ordered by type:**
`<tr ng-repeat="item in items | orderBy: 'title'">`

 ![](./figs/1P27_3.jpg)
 
    * **Ordered by description:**
`<tr ng-repeat="item in items | orderBy: 'body'">`

 ![](./figs/1P27_4.jpg)

**P29**
-	**Check that your page is still valid**

![](./figs/1P29_1.jpg)

**P33**
-	**Reload your page. Give a new auction -title, … Click on "add" to add it. What happens?**

![](./figs/1P33_1.jpg)

**P37**
-	**Reload your page. Try to remove an auction. What happens?**

![](./figs/1P37_1.jpg)

If I click on the icone trash, the whole line will disappear.
-	**Reload again the page. What happens? Why?**

![](./figs/1P37_2.jpg)

If I reload again the page, the disappearing line appeared again. Because every time I reload the page, it will show the things defined in auctionController. Our remove action can’t change the content of auctionController. 

#### <center>Step 2 : Add NodeJS</center>
**P11 Check that it works**
-	**Give a screenshot of the node.js result**

  ![](./figs/2P11_1.jpg)
  
**P16 Create a server**
-	**Give a screenshot of your web browser page**

 ![](./figs/2P16_1.jpg)
 
**P18**
- **Give a screenshot of web browser page**
**http://127.0.0.1:8080**

 ![](./figs/2P18_1.jpg)

**What happens with following URLs:**

http://127.0.0.1:8080/Hello

 ![](./figs/2P18_2.jpg)
 
http://127.0.0.1:8080/Hello.php

 ![](./figs/2P18_3.jpg)
 
http://127.0.0.1:8080/Hello.exe

 ![](./figs/2P18_4.jpg)
 
**Why such results?**

Every time we send a request to the server, no matter what's the request, it returns the same result.

**P19**
- **Explain what does these functions:**
 * **writeHead**
 
 `writeHead` writes the HTTP header
 
 * **write**

 `write` writes a response to the client

 * **end**
 
 `end` closes the response
 
**P21 Selecting routes**

- **Give a screenshot of web browser page**. **Test with several URLs**

 ![](./figs/2P21_1.jpg)
 
 ![](./figs/2P21_2.jpg)
  
 ![](./figs/2P21_3.jpg)
 
 ![](./figs/2P21_4.jpg)

- **Give a screenshot of the console**

 ![](./figs/2P21_5.jpg)

**P22**
- **Change server.js so that:**
	* **It displays a message if the path is**

		+ /
		
         ![](./figs/2P22_1.jpg)
        
		+ /Hello
		
        ![](./figs/2P22_2.jpg)
        
	* **It displays a 404 status otherwise**
 
 ![](./figs/2P22_3.jpg)
 
- **Give a screenshot of the web page with the results**

**P26**
- **Change the previous server.js file so that it idenGfies firstname and lastname. And display them if BOTH are present. Try it with
	- **Hello?firstName=yourFirstName&lastName=yourLastName**
	
    ![](./figs/2P26_1.jpg)
    
	- **Hello?firstName=lastName=yourLastName&yourFirstName**
	
    ![](./figs/2P26_2.jpg)
    
**P34**
- **Launch node with that script. What is the result?**

![](./figs/2P34_1.jpg)

- **Reload the page. What happens?**

![](./figs/2P34_2.jpg)

When reload the page, the number after the data is changed by adding one, it becomes 11.

- **One more time?**

![](./figs/2P34_3.jpg)

If reload the page one more time, the number return to 10.

**P42**
- **Test the script with html, css and image files. Add a screenshot**

html:
 
![](./figs/2P42_1.jpg)

css:
 
 ![](./figs/2P42_2.jpg)
 
 image files - png:
 
 ![](./figs/2P42_3.jpg)
 
 image files - jpg:
 
 ![](./figs/2P42_4.jpg)

#### <center>Step 3 : Add Express JS</center>
**P10 Add templates**
- **Add a screencopy of your browser**

 ![](./figs/3P10_1.jpg)

**P14**
- **How did you take into account the new parameter? Add a screencopy of your browser.**

	In the file **myView.ejs**, add `<%=nom%>` and  `<%=firstName %>` to take into account the new parameter.

 ![](./figs/3P14_1.jpg)

- **Does it still work with 1 parameter?
Did you forget something in the first route?
Add a screencopy of your browser**

 It doesn't work with 1 parameter. I need to add another parameter in the route: `http://127.0.0.1:8088/Hello/Mengxue/GUO`
 
 ![](./figs/3P14_2.jpg)
 
 **P17 Add paramater**
- **Test your server with http://127.0.0.1:8080/Hello/all. Oups. Did you place the route at its right place?
Modify the view to display only the 2 first ones.**

![](./figs/3P17_1.jpg)

In myView2.ejs:
```
<% for(var iname in names.slice(1)) { %>
	<li><%=names[iname] %></li>
<% } %>
```

**P23**
- **Add the image to your views. Test your server with every route. Make a sceencopy of your browser for each test**

 `http://127.0.0.1:8088/Hello/`:

 ![](./figs/3P23_1.jpg)

 `http://127.0.0.1:8088/Hello/all`:

 ![](./figs/3P23_2.jpg)

 `http://127.0.0.1:8088/Hello/Mengxue`:

 ![](./figs/3P23_3.jpg)

 `http://127.0.0.1:8088/Hello/Mengxue/GUO`:
 
 ![](./figs/3P23_4.jpg)
 
#### <center>Step 4 : Add a Database access</center>
**P11 Using PostgreSQL**
- **How can you check pg / mysql are installed?**
 In the folder `node_modules`, there is a folder `pg`, that means pg is installed.
 
- **Add a screencopy of your browser.**

 ![](./figs/4P11_1.jpg)
 
**P12**  <p style="color:red;">Not sure here</p>  
- **Add categories to your request and display them in your template. Add a screencopy of your browser**

  ![](./figs/4P12_1.jpg)
  
  or
  
  ![](./figs/4P12_2.jpg)

#### <center>Step 5 : Let's play with all</center>
**P6**
- **Open your browser and open a local connection
on port 3000. http://localhost:3000. Add a screencopy of your browser**

 ![](./figs/5P6_1.jpg)

**P13**
- **Relaunch npm. Open your browser and open a local connection on port 3000. http://localhost:3000. Add a screencopy of your browser**

 ![](./figs/5P13_1.jpg)
 
**P25**
- **Relaunch npm. Open your browser and open a local connection on port 3000. http://localhost:3000. Check that the information displayed is the one in your database. Add a screencopy of your browser**

 ![](./figs/5P25_1.jpg)
 
**P32**
- **Relaunch npm. Open your browser and open a local connection on port 3000. List items. Add new items. Check that the information displayed is the one in your database. Add a screencopy of your browser**
 
  ![](./figs/5P32_1.jpg)
  
**P36**
- **Relaunch npm. Open your browser and open a local connection on port 3000. Add a screencopy of your browser. **
 
  ![](./figs/5P36_1.jpg)

- **Did you notice something wrong with the ID? Do you remember item and category have both a column ID. How could you change that? Add a screencopy of your browser.**

 I find in the Auction column, the id of item turns to be the id of category.
 
 We need to change the SQL request to : 
```
 SELECT item.*, category.name FROM item LEFT OUTER JOIN category ON (item.category_id=category.id) ORDER BY item.id
```

 ![](./figs/5P36_2.jpg)  
 
**P42**
- **Relaunch npm. Open your browser and open a local connection on port 3000.  Add a screencopy of your browser. Add an item with a category.
Add another one without category.
Add a screencopy of your browser
Add a screencopy of your database to ensure
they were added.**

!!TODO

 ![](./figs/5P42_1.jpg)

 ![](./figs/5P42_2.jpg)

 ![](./figs/5P42_2.jpg)
 
**P47**
- **Relaunch npm. Open your browser and open a local connection on port 3000. Add a screencopy of your browser. Remove the items you added and make a
screencopy of the result. Add a screencopy of your database to ensure they were deleted.**

!!TODO

 ![](./figs/5P47_1.jpg)

After remove:

 ![](./figs/5P47_2.jpg)

In the database:

 ![](./figs/5P47_2.jpg)
 
 
  
